### Implementation of Towards Radiologist-Level Accurate Deep Learning System for Pulmonary Screening: https://arxiv.org/pdf/1807.03120.pdf

* We had to refer to the following papers to solve some ambiguities which were not written in the paper:
- Rethinking Convolutional Semantic Segmentation: https://arxiv.org/pdf/1710.07991.pdf 
- Traffic Sign Classification Using Deep Inception Based Convolutional Networks: https://arxiv.org/pdf/1511.02992.pdf 
- Rethinking the Inception Architecture for Computer Vision: https://arxiv.org/pdf/1512.00567.pdf 

