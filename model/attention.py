import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import math


class PartialAttention(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(PartialAttention, self).__init__()
        self.identity_layer = lambda x: x
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size = 3, stride = 1, padding=1)

    def forward(self, x, prev):
        #print("Dimension of x is ", x.size())
        height_u = list(x.size())[2]
        height_e = []
        for i, element in enumerate(prev):
            height_e.append(list(element.size())[2])
        height_e = np.array(height_e)/height_u
        concat = [x]

        N, C_x, W_x, H_x = x.size()
        for i, element in enumerate(height_e):
            _, C_i, W_i, H_i = prev[i].size()
            #print(W_x, W_i)
            P = ((W_x - 1)*int(element) + 3 - W_i)/2
            #print("Padding size is ", P)
            #print("Dimension of {0} is ".format(i), prev[i].size())
            if element < 0:
                continue
            elif element == float(1):
                #print(element, float(element))
                concat.append(prev[i])
            elif element > 1:
                concat.append(F.max_pool2d(prev[i], kernel_size = 3, stride = int(element), padding = math.ceil(P)))
        concat = torch.cat(concat, dim=1)
        return self.conv(concat)


"""
M = (N + 2*P - 3)/2 + 1

P = ((M - 1)*2 + 3 - N)/2

P = ((51)*2 + 3 - 108)/2 = -1.5

"""
