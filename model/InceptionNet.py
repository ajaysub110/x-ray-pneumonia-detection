
import os
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import attention
from attention import PartialAttention
import pickle

class ResidualInception(nn.Module):
    def __init__(self, in_channels, out_channels, b_1, b_2, b_out, c_1, c_2, c_out, d_1, d_out):
        super(ResidualInception,self).__init__()

        self.branch_a_res = nn.Conv2d(in_channels,out_channels,kernel_size=1) # 1X1 Residual Convolution
        nn.init.kaiming_uniform_(self.branch_a_res.weight)
        self.prelu_res = nn.PReLU()
        self.inception_batch_norm_res = nn.BatchNorm2d(out_channels)

        #In the following branches, the size of the matrix cannot decrease, since upsizing through convolution is not mentioned in the paper
        #It sounds more extreme to use upsampling to restore dimenions later than just padding to keep the dimenions same.

        self.branch_b_1 = nn.Conv2d(in_channels, b_1, kernel_size=1)
        self.prelu_b_1 = nn.PReLU()
        nn.init.kaiming_uniform_(self.branch_b_1.weight)
        self.batch_norm_b_1 = nn.BatchNorm2d(b_1)
        self.branch_b_2 = nn.Conv2d(b_1, b_2, kernel_size=(1,3), padding = (1,1))
        nn.init.kaiming_uniform_(self.branch_b_2.weight)
        self.prelu_b_2 = nn.PReLU()
        self.batch_norm_b_2 = nn.BatchNorm2d(b_2)
        self.branch_b_3 = nn.Conv2d(b_2, b_out, kernel_size=(3,1))
        nn.init.kaiming_uniform_(self.branch_b_3.weight)
        self.prelu_b_3 = nn.PReLU()
        self.batch_norm_b_3 = nn.BatchNorm2d(b_out) #B branch reduces dimension by 2,2 without any padding


        self.branch_c_1 = nn.Conv2d(in_channels, c_1, kernel_size = (1,1))
        nn.init.kaiming_uniform_(self.branch_c_1.weight)
        self.prelu_c_1 = nn.PReLU()
        self.batch_norm_c_1 = nn.BatchNorm2d(c_1)
        self.branch_c_2 = nn.Conv2d(c_1, c_2, kernel_size = (3,3), padding = (1,1))
        nn.init.kaiming_uniform_(self.branch_c_2.weight)
        self.prelu_c_2 = nn.PReLU()
        self.batch_norm_c_2 = nn.BatchNorm2d(c_2)
        self.branch_c_3 = nn.Conv2d(c_2, c_out, kernel_size = (3,3), padding = (1,1))
        nn.init.kaiming_uniform_(self.branch_c_3.weight)
        self.prelu_c_3 = nn.PReLU()
        self.batch_norm_c_3 = nn.BatchNorm2d(c_out) #C Branch reduces dimension by 4,4 without padding

        self.branch_d_1 = nn.Conv2d(in_channels, d_1, kernel_size = (1,1))
        nn.init.kaiming_uniform_(self.branch_d_1.weight)
        self.prelu_d_1 = nn.PReLU()
        self.batch_norm_d_1 = nn.BatchNorm2d(d_1)
        self.branch_d_2 = nn.Conv2d(d_1, d_out, kernel_size = (3,3), padding=(1,1))
        nn.init.kaiming_uniform_(self.branch_d_2.weight)
        self.prelu_d_2 = nn.PReLU()
        self.batch_norm_d_2 = nn.BatchNorm2d(d_out)
        self.branch_d_3 = nn.MaxPool2d(kernel_size = (3,3), stride = (1,1), padding = (1,1)) #D branch reduces dimension by 4,4 without padding

        self.conv_e = nn.Conv2d(d_out + c_out + b_out, out_channels, kernel_size = (3,3), padding = (1,1))
        nn.init.kaiming_uniform_(self.conv_e.weight)
        self.prelu_e = nn.PReLU()
        self.batch_norm_e = nn.BatchNorm2d(out_channels)

    def forward(self,x):
        branch_a_res = self.branch_a_res(x)
        branch_a_res = self.prelu_res(self.inception_batch_norm_res(branch_a_res))

        branch_b = self.prelu_b_1(self.batch_norm_b_1(self.branch_b_1(x)))
        branch_b = self.prelu_b_2(self.batch_norm_b_2(self.branch_b_2(branch_b)))
        branch_b = self.prelu_b_3(self.batch_norm_b_3(self.branch_b_3(branch_b)))

        branch_c = self.prelu_c_1(self.batch_norm_c_1(self.branch_c_1(x)))
        branch_c = self.prelu_c_2(self.batch_norm_c_2(self.branch_c_2(branch_c)))
        branch_c = self.prelu_c_3(self.batch_norm_c_3(self.branch_c_3(branch_c)))

        branch_d = self.prelu_d_1(self.batch_norm_d_1(self.branch_d_1(x)))
        branch_d = self.prelu_d_2(self.batch_norm_d_2(self.branch_d_2(branch_d)))
        branch_d = self.branch_d_3(branch_d)

        concat_e = [branch_b, branch_c, branch_d]
        concat_e = torch.cat(concat_e, dim=1)

        conv_e = self.conv_e(concat_e)
        conv_e = self.prelu_e(self.batch_norm_e(conv_e))

        output = conv_e + branch_a_res
        return output

class Net(nn.Module):
    def __init__(self, in_channels):
        super(Net,self).__init__()

        #input size = N, 3, 244, 244
        #output of attention layers is same as the input given to it.
        self.conv_1 = nn.Conv2d(in_channels = in_channels, out_channels=64, kernel_size = 5, stride = 2) #output size = N, 64, 110, 110
        self.batch_norm_1 = nn.BatchNorm2d(64)
        self.maxpool_1 = nn.MaxPool2d(kernel_size=3,stride=1) #output size = N, 64, 108, 108

        self.conv_2 = nn.Conv2d(64,192,kernel_size=3,stride=1) #output size = N, 192, 106, 106
        #self.attention_2 = PartialAttention(192, 192+64)
        self.batch_norm_2 = nn.BatchNorm2d(192) #output size = N, 192, 106, 106

        self.maxpool_2 = nn.MaxPool2d(kernel_size=3,stride=2) #output size = N, 192, 52, 52

        self.mIncept_3 = ResidualInception(192, 288, 96, 128, 128, 16, 32, 32, 64, 64) #output size = N, 288, 52, 52
        self.attention_3 = PartialAttention(288+192, 288)
        self.mIncept_4 = ResidualInception(288, 480, 128, 192, 192, 32, 96, 96, 64, 64) #output size = N, 480,
        self.attention_4 = PartialAttention(480+288+192, 480)
        self.maxpool_3 = nn.MaxPool2d(kernel_size=3,stride=2) #output size = N, 480, 25, 25

        self.mIncept_5 = ResidualInception(480, 512, 96, 208, 208, 16, 48, 48, 48, 64) #output size = N, 512, 25, 25
        self.attention_5 = PartialAttention(512+480+288+192, 512)
        self.mIncept_6 = ResidualInception(512, 512, 112, 224, 224, 24, 64, 64, 48, 64) #output size = N, 512, 25, 25
        self.attention_6 = PartialAttention(512+512+480+288+192, 512)
        self.mIncept_7 = ResidualInception(512, 512, 128, 256, 256, 24, 64, 64, 64, 64) #output size = N, 512, 25, 25
        self.attention_7 = PartialAttention(512+512+512+480+288+192, 512)
        self.mIncept_8 = ResidualInception(512, 528, 144, 288, 288, 32, 64, 64, 48, 64) #output size = N, 528, 25, 25
        self.attention_8 = PartialAttention(528+512+512+512+480+288+192, 528)
        self.mIncept_9 = ResidualInception(528, 832, 160, 320, 320, 32, 128, 128, 48, 128) #output size = N, 832, 25, 25
        self.attention_9 = PartialAttention(832+528+512+512+512+480+288+192, 832)
        self.maxpool_4 = nn.MaxPool2d(kernel_size=3, stride=2) #output size = N, 832, 12, 12

        self.mIncept_10 = ResidualInception(832, 832, 160, 320, 320, 32, 128, 128, 48, 128) #output size = N, 832, 12, 12
        self.attention_10 = PartialAttention(832+832+528+512+512+512+480+288+192, 832)
        self.mIncept_11 = ResidualInception(832, 1024, 192, 320, 320, 48, 128, 128, 32, 256) #output size = N, 1024, 12, 12
        self.attention_11 = PartialAttention(1024+832+832+528+512+512+512+480+288+192, 1024)
        self.avg_pool = nn.AvgPool2d(kernel_size=4) #output size = N, 1024, 3, 3

        #self.maxpool_final = nn.MaxPool2d(kernel_size=3)

        self.dropout = nn.Dropout2d(0.15) #N,1024,3,3
        self.linear = nn.Linear(in_features=1024*9,out_features=2,bias=True)
        self.softmax = nn.Softmax()

    def forward(self, x):
        prev = []

        x = self.conv_1(x)
        x = self.maxpool_1(x)

        x = self.conv_2(x)
        #x = self.attention_2(x, prev)

        x = self.maxpool_2(x)
        prev.append(x)

        x = self.mIncept_3(x)
        x = self.attention_3(x, prev)
        prev.append(x)

        x = self.mIncept_4(x)
        x = self.attention_4(x, prev)
        prev.append(x)

        x = self.maxpool_3(x)


        x = self.mIncept_5(x)
        x = self.attention_5(x, prev)
        prev.append(x)

        x = self.mIncept_6(x)
        x = self.attention_6(x, prev)
        prev.append(x)

        x = self.mIncept_7(x)
        x = self.attention_7(x, prev)
        prev.append(x)

        x = self.mIncept_8(x)
        x = self.attention_8(x, prev)
        prev.append(x)

        x = self.mIncept_9(x)
        x = self.attention_9(x, prev)
        prev.append(x)

        x = self.maxpool_4(x)


        x = self.mIncept_10(x)
        x = self.attention_10(x, prev)
        prev.append(x)

        x = self.mIncept_11(x)
        x = self.attention_11(x, prev)
        prev.append(x)


        x = self.avg_pool(x) #output size = N,1024,3,3

        #x = self.maxpool_final(x)

        x = x.view(-1, 9*1024) #N,9,1024
        x = self.dropout(x) 
        x = self.linear(x) #output size = N,1
        x = self.softmax(x)

        return x

net = Net(1)

import os 
import torch
import torchvision 
from torchvision import transforms, datasets
from torch.utils.data import Dataset, DataLoader
import pandas as pd 
import numpy as np 
import pydicom
from PIL import Image

data_path = './'
train_images_path = os.path.join(data_path,'train/')
test_images_path = os.path.join(data_path, 'test/')
train_labels_path = os.path.join(data_path,'stage_1_train_labels.csv')

train_transform = transforms.Compose([
    transforms.Resize(256),
    transforms.RandomResizedCrop(size=224),
    transforms.RandomHorizontalFlip(),
    transforms.RandomVerticalFlip(),
    transforms.ToTensor(),
    transforms.Normalize((0,0),(1,1))
]
)

class RSNADataset(Dataset):

    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.train_labels = pd.read_csv(csv_file, engine='python').drop(['x','y','width','height'],axis=1).T.reset_index(drop=True).T
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.train_labels)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir, self.train_labels.iloc[idx, 0]) + '.dcm'
        image = pydicom.dcmread(img_name).pixel_array
        image = Image.fromarray(image)
        label = self.train_labels.iloc[idx,1]

        if self.transform:
            image = self.transform(image)

        return image,label

def accuracy(outputs,labels):
    preds = outputs.max(dim=1)[1]
    return (preds==labels).float().sum()

tfmd_dataset = RSNADataset(csv_file=train_labels_path,root_dir=train_images_path,
        transform=train_transform)

dataloader = DataLoader(tfmd_dataset, batch_size=16, shuffle=True, num_workers=1)

criterion = nn.CrossEntropyLoss()

optimizer = optim.Adam(net.parameters(), lr=0.00001)

if torch.cuda.is_available():
    net.cuda()
    nn.DataParallel(net)
    print("GPU")
epoch_loss_data = []
epoch_accuracy_data = []

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

print(device)

# Loading old model
#net.load_state_dict(torch.load('checkpoint'))
#optimizer.load_state_dict(torch.load('optimizer_checkpoint'))

for epoch in range(10):  # loop over the dataset multiple times

    running_loss, running_loss_total,epoch_accuracy = 0.0, 0.0,0.0
    for i, data in enumerate(dataloader, 0):
        # get the inputs
        inputs, labels = data

        inputs, labels = inputs.to(device), labels.to(device)

        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)

        loss.backward()
        optimizer.step()

        # print statistics
        epoch_accuracy += accuracy(outputs,labels)
        running_epoch_acc = epoch_accuracy/(labels.size()[0] * (i+1))
        running_loss += loss.item()
        running_loss_total += loss.item()
        if i%30 == 29:
            print('[%d, %5d] loss: %.3f, epoch_accuracy: %f' % (epoch + 1, i + 1,running_loss,running_epoch_acc))
            running_loss = 0.0
        if i%100 == 99:
            torch.save(net.state_dict(), 'checkpoint')
            torch.save(optimizer.state_dict(), 'optimizer_checkpoint')
            with open('Running_loss.p','wb') as f:
                pickle.dump(running_loss_total,f)
            with open('Running_accuracy.p','wb') as f:
                pickle.dump(running_epoch_acc,f)
    epoch_accuracy_data.append(running_epoch_acc)
    epoch_loss_data.append(running_loss_total)


with open('Epoch_loss_1.p', 'wb') as file:
    pickle.dump(epoch_loss_data, file)
with open('Epoch_accuracy_1.p','wb') as file:
    pickle.dump(epoch_accuracy_data,file)

print('Finished Training')
